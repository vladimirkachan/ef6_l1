﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1
{
    class Program
    {
        static void Main(string[] args)
        {
            using ef6_l1Entities db = new ();
            foreach(var i in db.Tables.ToList())
                Console.WriteLine($"{i.FirstName} {i.LastName} {i.Age} years old.");
            Console.ReadKey();
        }
    }
}
