﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            using Model1Container db = new();
            //// Generate(db);
            foreach(var i in db.PersonalInfoSet.ToList().OrderBy(p => p.Age))
                Console.WriteLine($"{i.FirstName} {i.LastName} {i.Age} years old.");
            Console.ReadKey();
        }
        static void Generate(Model1Container db)
        {
            db.PersonalInfoSet.Add(new PersonalInfo {FirstName = "Anna", LastName = "Malinova", Age = 33});
            db.PersonalInfoSet.Add(new PersonalInfo {FirstName = "Ulya", LastName = "Klimova", Age = 22});
            db.SaveChanges();
        }
    }
}
