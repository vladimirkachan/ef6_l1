﻿namespace L1_3
{
    public class PersonalInfo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName {get; set;}
        public int Age {get; set;}
    }
}