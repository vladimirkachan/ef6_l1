﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            using Model1 db = new();
            var persons = db.Persons.ToList();
            foreach (var p in persons)
                Console.WriteLine($"{p.FirstName,-8} {p.LastName,-15} {p.Age} years old.");
            Console.ReadKey();
        }
    }
}
