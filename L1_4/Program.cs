﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_4
{
    class Program
    {
        static void Main(string[] args)
        {
            using DataModel db = new();
            Console.WriteLine($"ConnectionString = {db.Database.Connection.ConnectionString}");
            foreach(var i in db.Persons.ToList())
                Console.WriteLine($"{i.FirstName} {i.LastName} {i.Age} years old");
            Console.ReadKey();
        }
    }
}
